﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace life
{
    class Program
    {
        static void Main(string[] args)
        {
           
            LifeBoard lf = new LifeBoard(15);
            lf.symulate();
        }
    }

    class LifeBoard
    {
        private int size;
        private bool[,] board;

        public LifeBoard(int size)
        {
            this.size = size;
            board = new bool[size, size];
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    board[i, j] = (rand.Next(2) == 0);                    
                }
            }
        }
        public void print()
        {
            Console.Clear();
            for(int i=0;i< size; i++)
            {
                for (int j = 0; j < size; j++) {
                    if (board[i, j])
                    {
                        Console.Write("x");
                    }
                    else
                    {
                        Console.Write(".");
                    }
                    Console.Write(" ");
                }
                Console.Write("\n");
            }
        }

        public bool[,] calculateBoard()
        {
            bool[,] newBoard = new bool[size, size];

            //dla wszystkich komórek
            for (int x = 0; x < size; x++) {
                for (int y = 0; y < size; y++) {
                    //zlicz sasiadów
                    int counter = 0;
                    for (int i = x - 1; i <= x + 1; i++)
                    {
                        for (int j = y - 1; j <= y + 1; j++)
                        {
                            if (!(x == i && y == j)) {
                                if (!outOfBound(i, j)
                                    && board[i, j]
                                    )
                                {
                                    counter++;
                                }
                            } }
                    }//koniec zliczania

                    //jesli żywa
                    if (board[x, y])
                    {
                        if (counter < 2 || counter > 3)
                        {
                            newBoard[x, y] = false;
                        }
                        else {
                            newBoard[x, y] = true;
                        }
                    }
                    else //jeśli martwa
                    {
                        if (counter == 3)
                        {
                            newBoard[x, y] = true;
                        }
                    }
                }
            }
            //zwróć nową
            return newBoard;
        }

        public void symulate()
        {
            do
            {
               this.print();
               this.board = this.calculateBoard();
               Console.Read();
            } while (true);
        }

        public bool outOfBound(int x, int y)
        {
            if ((x >= this.size) || (y >= this.size) || (x < 0) || (y < 0))
            {
                return true;
            }
            else {
                return false;
            }

        }

    }
}
